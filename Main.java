import java.util.Collections;

/**
 * Application class for testing Alice in Wonderland article.
 * @author Raksit Mantanacharu
 *
 */
public class Main {
	/**
	 * Main method.
	 * @param args as Main.
	 */
	public static void main (String[] args) {
		WordCounter counter = new WordCounter();
		String[] wordArr = counter.getSortedWords();
		for(int i = 0; i < 20; i++) {
			System.out.println(wordArr[i] + " = " + counter.getCount(wordArr[i]));
		}
	}
}
