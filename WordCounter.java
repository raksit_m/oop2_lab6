import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * Counting the words in the Alice in Wonderland.
 * @author Raksit Mantanacharu
 *
 */
public class WordCounter {
	/** Map for collecting the words as keys and occurrences as values. */
	private HashMap<String, Integer> wordGroup;
	/** Alice in Wonderland reference. */
	private String fileURL = "https://bitbucket.org/skeoop/oop/raw/master/week6/Alice-in-Wonderland.txt";
	private URL url;
	private InputStream input;
	private List<String> wordList;
	
	/**
	 * Constructor:
	 * - Initializing HashMap
	 * - Using Scanner for getting an input from reference
	 * - Removing Punctuation from the article
	 * - Adding words, ignoring case.
	 * 
	 */
	public WordCounter() {
		wordGroup = new HashMap<String, Integer>();
		try {
			url = new URL(fileURL);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		try {
			input = url.openStream();
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		Scanner scanner = new Scanner(input);
		final String delims = "[\\s,.\\?!\"():;]+";
		scanner.useDelimiter(delims);
		
		wordList = new ArrayList<String>();
		
		while(scanner.hasNext()) {
			wordList.add(scanner.next().toLowerCase());
		}
		scanner.close();
		
		for(int i = 0; i < wordList.size(); i++) {
			addWord(wordList.get(i));
		}
	}
	
	/**
	 * Adding one to the count of occurrences to the Map.
	 * @param word which we would like to see the occurrences.
	 */
	public void addWord(String word) {
		int value = getCount(word);
		wordGroup.put(word, value);
	}
	
	/**
	 * Use it for sorting, see it below.
	 * @return Set of words in the article.
	 */
	public Set<String> getWords() {
		return wordGroup.keySet();
	}
	
	/**
	 * Checking the occurrences of the word as a parameter.
	 * @param word which we would like to count
	 * @return how many times which word occurs in the article
	 */
	public int getCount(String word) {
		int count = 0;
		for(int i = 0; i < wordList.size(); i++) {
			if(wordList.get(i).equalsIgnoreCase(word)) {
				count++;
			}
		}
		return count;
	}
	
	/**
	 * Sort the words from getWords().
	 * @return array for alphabetically sorted words
	 */
	public String[] getSortedWords() {
		List<String> list = new ArrayList<String>(getWords());
		Collections.sort(list);
		String[] wordArr = new String[list.size()];
		list.toArray(wordArr);
		return wordArr;
	}
	
	/**
	 * 
	 * @return Map
	 */
	public HashMap<String, Integer> getMap() {
		return this.wordGroup;
	}
	
}
